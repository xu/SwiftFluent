import struct Foundation.URL
import SwiftFluent

let manager = FluentLocalizationManager(scheme: "locales/{locale}/{res_id}")

let l10n = manager.loadLocalization(["zh-Hans", "en"], resources: .all)

// #SE-0195 
// output: "你好，世界！"
l10n.hello_world()
l10n["hello-world"]()

// #SE-0216
// output: "Hello, Tom!"
l10n.greeting(name: "Tom")
l10n["greeting"](["name": "Tom"])

let standaloneResource = FluentResource.loadFromURL(URL(string: "locales/os-greeting.ftl"))
l10n.addResource(standaloneResource)

let env = FluentEnvironment()
env.registerFunction(name: "OS") {
    #if os(macOS)
        return "macOS"
    #elseif os(Linux)
        return "linux"
    #elseif os(Windows)
        return "windows"
    #endif
}
let l10nWithEnv = l10n.withEnvironment(env)

// output on macOS: "欢迎来到macOS!"
l10nWithEnv.os_greeting()
