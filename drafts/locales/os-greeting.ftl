os-greeting = { OS() ->
    [linux]     Welcome to Linux
    [macOS]     Welcome to macOS
    [windows]   Welcome to Windows
   *[other]     Welcome
   }